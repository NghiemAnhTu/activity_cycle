package com.rikkei.tranning.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {
    Button btnActivityA;
    MediaPlayer mp3B;
    int timeCurrentA;
    int timeCurrentB;
    @Override
    protected void onStart() {
        Log.d("Activity Cycle", "onStart B");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d("Activity Cycle", "onStop B");
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        Log.d("Activity Cycle", "onDestroy B");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.d("Activity Cycle", "onPause B");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d("Activity Cycle", "onResume B");
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Activity Cycle", "onCreate B");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        // anh xa
        btnActivityA    =   (Button) findViewById(R.id.btn_intent);
        mp3B = MediaPlayer.create(Main2Activity.this, R.raw.test);
        // lay intent tu activity A
        final Intent activityBA = getIntent();
        timeCurrentA = activityBA.getIntExtra("dataA", 0);
        timeCurrentB = activityBA.getIntExtra("dataB", 0);
        //Log.d("TimeLineB", timeCurrentA + ", " + timeCurrentB);
        mp3B.seekTo(timeCurrentB);
        mp3B.start();
        // chuyen sang activity A
        btnActivityA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3B.pause();
                timeCurrentB = mp3B.getCurrentPosition();
                Intent dataReq = new Intent();
                dataReq.putExtra("dataA", timeCurrentA);
                dataReq.putExtra("dataB", timeCurrentB);
                setResult(Activity.RESULT_OK, dataReq);
                finish();
            }
        });
    }

    // xoay man hinh
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mp3B.pause();
        timeCurrentB = 0;
        mp3B.seekTo(timeCurrentB);
        mp3B.start();
    }
}
