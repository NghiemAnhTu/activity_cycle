package com.rikkei.tranning.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity {
    Button btnActivityB;
    MediaPlayer mp3A;
    int MY_CODE = 100;
    int timeCurrentA = 0;
    int timeCurrentB = 0;
    @Override
    protected void onStart() {
        Log.d("Activity Cycle", "onStart A");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d("Activity Cycle", "onStop A");
        super.onStop();
        mp3A.stop();
    }

    @Override
    protected void onDestroy() {
        Log.d("Activity Cycle", "onDestroy A");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.d("Activity Cycle", "onPause A");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d("Activity Cycle", "onResume A");
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Activity Cycle", "onCreate A");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // anh xa
        btnActivityB     =   (Button) findViewById(R.id.btn_intent);
        // tao va phat nhac
        mp3A    =   MediaPlayer.create(MainActivity.this, R.raw.test);
        mp3A.start();
        // su kien chuyen activity
        btnActivityB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3A.pause();
                timeCurrentA = mp3A.getCurrentPosition();
                //Log.d("time", timeCurrentA + "");
                Intent activityAB = new Intent(MainActivity.this, Main2Activity.class);
                activityAB.putExtra("dataA", timeCurrentA);
                activityAB.putExtra("dataB", timeCurrentB);
                startActivityForResult(activityAB, MY_CODE);
            }
        });
    }

    // nhan data tu activity B
    protected void  onActivityResult( int requestCode, int resultCode, Intent data ){
        if (resultCode == Activity.RESULT_OK && requestCode == MY_CODE ) {
            timeCurrentA    =   data.getIntExtra("dataA", 0);
            timeCurrentB    =   data.getIntExtra("dataB", 0);
            mp3A.seekTo(timeCurrentA);
            mp3A.start();

        }
    }

    // xoay man hinh
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mp3A.pause();
        timeCurrentA = 0;
        mp3A.seekTo(timeCurrentA);
        mp3A.start();
    }
}
